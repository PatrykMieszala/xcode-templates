//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

import Foundation
import RxSwift
import ios-rxmvvm

protocol ___FILEBASENAME___Protocol {
    
}

final class ___FILEBASENAME___: ___FILEBASENAME___Protocol {
    
    // MARK: - Singleton Instance
    static let instance = ___FILEBASENAME___()
    
    // MARK: - Private Member(s)
    
    // MARK: - Public Member(s)
    
    // - MARK: Constructor(s)
    private init() {
        
    }
    
    // - MARK: Public Method(s)
    
    // - MARK: Private Method(s)
}
