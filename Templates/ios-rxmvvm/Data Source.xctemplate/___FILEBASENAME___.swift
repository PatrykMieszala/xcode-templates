//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources
import ios-rxmvvm

class ___FILEBASENAME___ {
    
    // - MARK: Public Variable(s)
    
    // - MARK: Private Variable(s)
    private let viewModel: <#ViewModel#>
    private let tableView: UITableView
    private let collectionView: UICollectionView
    
    private var tableDataSource: RxTableViewSectionedReloadDataSource<SectionModel<Void, <#CellViewModel#>>>?
    private var collectionDataSource: RxCollectionViewSectionedReloadDataSource<SectionModel<Void, <#CellViewModel#>>>?
    
    // - MARK: Constructor(s)
    
    required init(tableView: UITableView, collectionView: UICollectionView, viewModel: <#ViewModel#>) {
        self.viewModel = viewModel
        self.tableView = tableView
        self.collectionView = collectionView
    }
    
    // - MARK: Public Method(s)
    func bind() {
        tableView.dataSource = nil
        collectionView.dataSource = nil
        
        let tableDataSource = RxTableViewSectionedReloadDataSource<SectionModel<Void, <#CellViewModel#>>>(configureCell: { dataSource, tableView, indexPath, viewModel in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: <#T##String#>, for: <#T##IndexPath#>) !! "cell can't be nil"
            cell.viewModel = viewModel
            
            return cell
        })
        
        self.tableDataSource = tableDataSource
        
        let collectionDataSource = RxCollectionViewSectionedReloadDataSource<SectionModel<Void, <#CellViewModel#>>>(configureCell: { dataSource, collectionView, indexPath, viewModel in
            
            let cell = collectionView.dequeueReusableCell(withIdentifier: <#T##String#>, for: <#T##IndexPath#>) !! "cell can't be nil"
            cell.viewModel = viewModel
            
            return cell
        })
        
        self.collectionDataSource = collectionDataSource
        
        viewModel
            .feed
            .map { (elements) -> [SectionModel<Void, <#CellViewModel#>>] in
                return [SectionModel(model: (), items: elements)]
            }
            .bind(to: tableView.rx.items(dataSource: tableDataSource))
            .dispose(in: disposeBag)
        
        viewModel
            .feed
            .map { (elements) -> [SectionModel<Void, <#CellViewModel#>>] in
                return [SectionModel(model: (), items: elements)]
            }
            .bind(to: tableView.rx.items(dataSource: collectionDataSource))
            .dispose(in: disposeBag)
    }
}
