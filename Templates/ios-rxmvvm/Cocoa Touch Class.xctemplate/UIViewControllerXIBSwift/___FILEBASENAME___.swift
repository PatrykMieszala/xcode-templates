//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

import UIKit
import RxSwift
import RxCocoa
import ios-rxmvvm

private struct ___FILEBASENAMEASIDENTIFIER___Constants {
    
}

private typealias C = ___FILEBASENAMEASIDENTIFIER___Constants

class ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_cocoaTouchSubclass___ {
    
    // - MARK: ibOutlet(s)
    
    // - MARK: Public Variable(s)
    //var viewModel: ViewModel!
    
    // - MARK: Private Variable(s)
    
    
    // - MARK: View Method(s)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bind()
    }
}

// - MARK: private ___FILEBASENAMEASIDENTIFIER___ extension
private extension ___FILEBASENAMEASIDENTIFIER___ {
    
    // - MARK: Binding(s)
    func bind() {
        
    }
}
