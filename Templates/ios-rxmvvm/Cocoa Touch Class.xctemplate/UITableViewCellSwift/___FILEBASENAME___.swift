//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

import UIKit
import RxSwift
import RxCocoa
import ios-rxmvvm

private struct ___FILEBASENAMEASIDENTIFIER___Constants {
    
}

private typealias C = ___FILEBASENAMEASIDENTIFIER___Constants

class ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_cocoaTouchSubclass___ {
    
    // - MARK: ibOutlet(s)
    
    // - MARK: Public Variable(s)
    
    //var viewModel: ViewModel! {
    //    didSet {
    //       initialize()
    //    }
    //}
    
    // - MARK: View Method(s)
    private func initialize() {
        
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }

}
