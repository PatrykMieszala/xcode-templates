//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

import Foundation
import RxSwift
import ios-rxmvvm

protocol I___FILEBASENAME___ {
    
}

final class ___FILEBASENAME___: I___FILEBASENAME___ {
    
    private let api: <#IApiService#>
    
    // - MARK: Constructor(s)
    init(api: <#IApiService#>) {
        self.api = api
    }
    
    // - MARK: Public Method(s)
    
    // - MARK: Private Method(s)
}
