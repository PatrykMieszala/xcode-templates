//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

import Foundation
import RxSwift
import ios-rxmvvm

protocol I___FILEBASENAME___: <#Protocol#> {
    
}

class ___FILEBASENAME___: I___FILEBASENAME___ {
    
    // - MARK: Public Variable(s)
    
    // - MARK: Private Variable(s)
    private let service: <#Service#>
    
    // - MARK: Constructor(s)
    init(service: <#Service#>) {
        self.service = service
    }
    
    // - MARK: Public Method(s)
    
    // - MARK: Private Method(s)
}
