//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

import Foundation
import ObjectMapper

class ___FILEBASENAME___: Mappable {
    
    // MARK: - Public Properties
    
    init() {
        
    }
    
    required init?(map: Map) {
    }
    
    // MARK: ObjectMapper
    func mapping(map: Map) {
        //id                                      <- map["id"]
    }
}
