//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

import UIKit
import RxSwift
import RxCocoa
import ios_rxmvvm

final class ___FILEBASENAME___: RxCoordinator {
    
    @IBOutlet fileprivate weak var navigationController: UINavigationController!
    
    convenience init(parent: RxCoordinator? = nil, navigationController: UINavigationController) {
        self.init(parent: parent)
        self.navigationController = navigationController
    }
    
    @discardableResult
    override func start() -> UIViewController? {
        //let vc = R.storyboard.name.viewController() !! "VC can't be nil"
        
        return nil
    }
}

private extension ___FILEBASENAME___ {
//    var navigateTo: Route<Void> {
//        return route(parent: self) { (this, data) in
//            return data
//        }
//    }
}
