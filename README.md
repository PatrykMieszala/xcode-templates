### What is this repository for? ###

* Templates for XCode. Enjoy ready-to-use templates for MVVM pattern adjusted for Bond/RxSwift.

### How do I get set up? ###

* Just copy `Templates` folder into `/Libraries/Developer/XCode/`.